#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>

#define BUFFER_SIZE 10

int buffer[BUFFER_SIZE];
int read_index = 0;
int write_index = 0;
pthread_mutex_t lock;

int is_prime(int n)
{
    if (n <= 1)
        return 0;
    for (int i = 2; i * i <= n; i++)
    {
        if (n % i == 0)
            return 0;
    }
    return 1;
}

int randomInteger()
{
    return rand() % 100;
}

void *producer(void *arg)
{
    while (1)
    {
        int random_num = randomInteger();
        pthread_mutex_lock(&lock);
        buffer[write_index] = random_num;
        write_index = (write_index + 1) % BUFFER_SIZE;
        pthread_mutex_unlock(&lock);
        sleep(1);
    }
}

void *consumer(void *arg)
{
    while (1)
    {
        pthread_mutex_lock(&lock);
        int num = buffer[read_index];
        read_index = (read_index + 1) % BUFFER_SIZE;
        pthread_mutex_unlock(&lock);
        printf("Nombre consommé : %d\n", num);
        if (is_prime(num))
        {
            printf("%d est premier\n", num);
        }
        else
        {
            printf("%d n'est pas premier\n", num);
        }
        sleep(2);
    }
}

int main()
{
    pthread_t prod, cons;
    pthread_mutex_init(&lock, NULL);

    pthread_create(&prod, NULL, producer, NULL);
    pthread_create(&cons, NULL, consumer, NULL);

    pthread_join(prod, NULL);
    pthread_join(cons, NULL);

    pthread_mutex_destroy(&lock);

    return 0;
}